<?php



class GL_Body {
    public $Data;
    public $Parent;
                                                                       
    public function __construct($Obj = null) {
        $this->Parent = &$Obj;
        $this->Data = array();
    }
    
    public function __set($name, $value) {
        $this->Data[$name] = $value;
    }

    public function __get($name) {
        if (!is_array($this->Data)) {
            pre($name);
        }
        if (array_key_exists($name, $this->Data)) {
            return $this->Data[$name];
        }else{
            return false;
        }
    }

    public function __isset($name) {
        return isset($this->Data[$name]);
    }

    public function __unset($name) {
        unset($this->Data[$name]);
    }
}
include('Settings.php');
include(GL_MODULE_PATH.'Collision2D.php');
include(GL_MODULE_PATH.'Utils.php'); 
include(GL_MODULE_PATH.'Objects.php');
include(GL_MODULE_PATH.'GUI.php'); 
include(GL_MODULE_PATH.'Camera.php');
include(GL_MODULE_PATH.'Text.php');
include(GL_MODULE_PATH.'Loader.php');
include(GL_MODULE_PATH.'Physics.php');
include(GL_MODULE_PATH.'Sound.php');
include(GL_MODULE_PATH.'Controls.php');

?>