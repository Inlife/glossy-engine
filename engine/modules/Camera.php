<?php

class GL_Camera extends GL_Body {

    public function __construct($Obj) {
    
        parent::__construct($Obj);
        
    }
    
    public function SetTarget($Obj) {
    
        $this->TargetObj = $Obj;
        $this->X = $Obj->X - $this->Parent->ResX / 2;
        $this->Y = $Obj->Y - $this->Parent->ResY / 2;
        
    }
    
    public function Update() {
    
        $this->SetTarget($this->TargetObj);
    
    }
    
    public function CheckFrame() {
                                   
        /* �������� ������� ������*/ 
        $Obj = $this->TargetObj;    
        $sum = $this->X - $Obj->X;
        if ($sum < -500) {
        
            $this->X += $Obj->XSpeed;
            
        }elseif ($sum > -300) {
        
            $this->X += $Obj->XSpeed;
            
        }     
        $sum = $this->Y - $Obj->Y; 
        if ($sum < -350) {
        
            $this->Y += round($Obj->YSpeed);
            
        }elseif ($sum > -250) {
        
            $this->Y += $Obj->YSpeed;
            
        }       
    }        
}

?>