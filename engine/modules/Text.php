<?php


class GL_Text extends GL_Body {
    
    public $GL_CACHE;
    
    public function __construct($Obj) {
        
        parent::__construct($Obj);
        $this->GL_CACHE = array();
      
    }
    
    public function String($Str, $X, $Y, $Layer, $Size = 20.0, $Angle = 0, $Color = '255,255,255', $BackColor = '0,0,0') {
       
        $Texture = $this->GetImage($Str, $Size, $Color, $BackColor);
        $this->Parent->Image(false, $Texture, $X, $Y, strlen($Str) * ($Size * 0.7), $Size * 1.5, $Layer, 1, 255, $Angle);
    
    }
    
    public function GetImage($Str, $Size, $Color, $BackColor) {
    
        $Name =  GL_CACHE_INAME . md5($Str . $Size . $Color . $BackColor);
        if ($this->GL_CACHE[$Name]) {
            
            return $this->GL_CACHE[$Name];
        
        }elseif (file_exists(GL_CACHE_DIR . $Name . GL_CACHE_IFILETYPE)) {
            
            return $this->LoadImage($Name);
        
        }else{
            
            return $this->DrawImage($Str, $Size, $Color, $BackColor);
        
        }
    
    }
    
    public function LoadImage($Name) {
    
        $this->GL_CACHE[$Name] = Tex_LoadFromFile(GL_CACHE_DIR . $Name . GL_CACHE_IFILETYPE, 0, TEX_DEFAULT_2D);
        return $this->GL_CACHE[$Name];
    
    }
    
    public function DrawImage($Str, $Size, $Color, $BackColor) {
    
        $Name = GL_CACHE_INAME . md5($Str . $Size . $Color . $BackColor);
        $W = strlen($Str) * ($Size * 0.7);
        $H = $Size * 1.5;
        $GL_TC = explode(',', $Color); 
        $GL_BC = explode(',', $BackColor); 
        $Font = GL_FONTNAME;
        $im = imagecreatetruecolor($W, $H);
        $GL_CTextColor = imagecolorallocate($im, $GL_TC[0], $GL_TC[1], $GL_TC[2]);
        $GL_CBackColor = imagecolorallocate($im, $GL_BC[0], $GL_BC[1], $GL_BC[2]); 
        imagefilledrectangle($im, 0, 0, $W - 1, $H - 1, $GL_CBackColor);
        imagecolortransparent($im, $GL_CBackColor);  
        imagettftext($im, $Size, 0, 0, $Size, $GL_CTextColor, $Font, iconv("WINDOWS-1251", "UTF-8", $Str));
        imagegif($im, GL_CACHE_DIR . $Name . GL_CACHE_IFILETYPE);
        imagedestroy($im);
        return $this->LoadImage($Name);
        
    
    }

}


?>
