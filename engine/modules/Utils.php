<?php

abstract class Utils {

    static function str2args($str, &$link) {
    
        $args = explode(',', trim($str));
        foreach ($args as $arg) {
        
            $fin = explode(':', $arg);
            if (trim($fin[0]) OR trim($fin[1])) { 
                
                $link->$fin[0] = $fin[1];
            
            }
        
        }
        
    }
    
    static function AnimMap($file, &$link) {
    
        if (file_exists($file)) {
        
             $text = file($file);
             foreach($text as $str) {
             
                 $o1 = explode("::", $str);
                 $o2 = explode(":", $o1[1]);                 
                 foreach($o2 as $num => $nus) {
                 
                     $o3 = explode("-", $nus);
                     $arr[$o1[0]][$num][0] = intval(trim($o3[0]));
                     $arr[$o1[0]][$num][1] = intval(trim($o3[1]));
                     
                 } 
                 
             }
             
             $link->AnimMap = $arr;
             
         }
    
    }
    
    static function Bar($text) {
    
        c('BAR')->simpleText = $text;
    
    }

}

?>