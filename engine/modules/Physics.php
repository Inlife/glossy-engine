<?php


class GL_Physics extends GL_Body {

    public function __construct($Obj) {
    
        parent::__construct($Obj);
        $this->Gravity = 1;
    
    }  
    
    public function CheckCollision($Obj, $X, $Y) {
           
        if ((($Obj->X + $X) - $Obj->W / 2) < 0 OR (($Obj->X + $X) + $Obj->W / 2 > $this->Parent->MapW)) {
            
            return true;
            
        }if ((($Obj->Y + $Y) - $Obj->H / 2) < 0 OR (($Obj->Y + $Y) + $Obj->H / 2 > $this->Parent->MapH)) {
        
            return true;
             
        }
        $Xz = ($Obj->X + $X) - $Obj->W / 2;
        $Yz = ($Obj->Y + $Y) - $Obj->H / 2; 
        $W = $Obj->W;
        $H = $Obj->H;
        $Box1 = new Box2D(P4D($Xz, $Yz, $W, $H));
    
        foreach($this->Parent->Objects as $Object) {
            
            if (($Obj->Name != $Object->Name) AND (abs($Obj->X - $Object->X) < $Object->W + 100) AND (abs($Obj->Y - $Object->Y) < $Object->W + 100))  {
                
                
                $X = $Object->X - $Object->W / 2;
                $Y = $Object->Y - $Object->H / 2; 
                $W = $Object->W;
                $H = $Object->H;
                $Box2 = new Box2D(P4D($X, $Y, $W, $H));
                if (Col2D::Box($Box1, $Box2)) {                
                    
                    if (!$Object->Walkable) {
                        
                        return true;
                    
                    }else{
                    
                        switch(get_class($Object)) {
                        
                            case 'GL_CheckPoint': 
                            
                                if (($Object->CPactive) && ($Obj->Name == $this->Parent->Player)) {
                                
                                    $Object->CPactive = 0;
                                    $Obj->lastCP = $Object->Name;
                                
                                }
                            
                            break;
                            case 'GL_HumanObject':
                            
                                $this->Parent->Text->String('���� �����������', 300, 200, 15);
                            
                            break;
                        
                        }
                    
                    }
                
                }            
                             
            }
        
        }
        
        return false;
    
    }
    
    public function Move($Obj, $dirX, $dirY) {
    
        $X = $Obj->XSpeed;
        $Y = $Obj->YSpeed;
        $colX = $this->CheckCollision($Obj, $X, 0);
        $colY = $this->CheckCollision($Obj, 0, $Y); 
        
        switch($dirX) {
            
            case 1:
            
                if (!$colX) {
                                      
                    $Obj->X += $X;
                     
                }
            
            break;            
            case -1:
            
                if (!$colX) {
                                      
                    $Obj->X += $X;
                     
                }
                
            
            break;
            
        }switch($dirY) {
            
            case 1:
            
                if (!$colY) {
                                      
                    $Obj->Y += $Y;
                
                }
            
            break;            
            case -1:
            
                if (!$colY) {
                                      
                    $Obj->Y += $Y;
                
                }
                            
            
            break;
            
        }
        
        return true;
    
    }
    
    public function Process($Obj) {
    
    
        /*
         * ��������� ����������: XSpeed, YSpeed, OSpeed - ����� ���� �������������� 
         */
        if ($Obj->Type == GL_OBJ_DYNAMIC) {  
        
            /* X */
            switch($Obj->Sprite->Status) {
              case A_STOP:
            
                if ($Obj->XSpeed > 0 ) {
                
                    $Obj->XSpeed -= $Obj->aX;
                
                }elseif ($Obj->XSpeed < 0) {
                
                    $Obj->XSpeed += $Obj->aX;
                
                }else{
                
                    $Obj->dirX = 0;
                
                }
            
              break;
              case A_WALK:
            
                switch($Obj->dirX) {
                
                    case 1:
                    
                        if ($Obj->XSpeed < $Obj->Xlimit) {
                        
                            $Obj->XSpeed += $Obj->aX;
                        
                        }
                     
                    break;
                    case -1:
                    
                        if ($Obj->XSpeed > (-1 * $Obj->Xlimit)) {
                        
                            $Obj->XSpeed -= $Obj->aX;
                        
                        }
                     
                    break;
                
                }  
            
            } 
            /* Y */
            switch($Obj->Jump) {
            
                case true:
            
                    switch($Obj->dirY) {
                    
                        case 1:
                        
                            if ($Obj->YSpeed < $Obj->Ylimit) {
                        
                                $Obj->YSpeed += $Obj->aY;
                        
                            }
                            $COL = $this->CheckCollision($Obj, 0, $Obj->YSpeed);
                            if ($COL) {
                                
                                $Obj->dirY = 0;
                                $Obj->Jump = false;
                                $Obj->YSpeed = 0;
                                $Obj->Sprite->Status(A_STOP);
                        
                            }
                     
                        break;
                        case -1:
                        
                            if ($Obj->YSpeed > (-1 * $Obj->Ylimit)) {
                            
                                $Obj->YSpeed -= $Obj->aY;
                        
                            }else{
                            
                                $Obj->dirY = 1;
                            
                            }
                        
                        break;
                    
                    }
            
                break;
                default:
                
                    if ($Obj->dirY == 0) {
                    
                        $COL = $this->CheckCollision($Obj, 0, $Obj->aY);
                        if (!$COL) {
                        
                            $Obj->dirY = 1;
                            $Obj->Jump = true;
                            $Obj->Sprite->Status(A_JUMP);
                        
                        }
                    
                    }
                
                break;
                
            }  
            $this->Move($Obj, $Obj->dirX, $Obj->dirY);   
        
        }
    
    }
    
    public function Flip($Obj) {
    
        /*
        x = x0 + Radius * cos(radianAngle); 
        y = y0 + Radius * sin(radianAngle);
        */
    
    }
    
}


?>