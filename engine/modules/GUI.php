<?php

class GL_GUI extends GL_Body  {
    
    public function __construct($obj) {
    
        parent::__construct($obj);
    
    }
    
    //public function 
    
}    
 

class GL_GUI_Form extends GL_ObjMain {

    public function __construct($GL, $params) {
        
        parent::__construct();
        $this->Sprite = false;
        $this->Active = false;
        $this->Parent = $GL;
        $this->Layer = 10;
        $this->Components = array(); 
        $this->Focus = false;
        Utils::str2args($params, $this);  
    
    }
    
    public function AddComponent($Obj) {
    
        $this->Components[$Obj->Name] = $Obj;
    
    }  

}

class GL_GUI_Button extends GL_ObjMain {

    public function __construct($params) {
        
        parent::__construct();
        $this->Sprite = false;
        $this->Hover = false;
        $this->Parent = false;
        $this->OnClick = '';
        Utils::str2args($params, $this);
        $this->Caption = 'But_'.$this->Name;   
    
    }
    
    public function Click() {
    
        eval($this->OnClick);
    
    } 
    
    public function Draw() {
    
        $S = $this->Sprite;
        if ($this->Hover) {
        
            $S->Frame = 2;
        
        }else{
        
            $S->Frame = 1;
        
        }
        $this->Parent->Parent->Image($S->Animated, $S->Texture, $this->X, $this->Y, $S->W,  $S->H, $this->Parent->Layer + 1, $S->Frame);
        $this->Parent->Parent->Font->Write($this->X + $this->W / 2, $this->Y + $this->H / 2, $this->Caprion, 10, $this->Parent->Layer + 2); 
    }

} 

?>