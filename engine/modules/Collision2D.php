<?php
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////                                                                                                                                                                                                                        ////
////     :���������.                                  .:.                                                          (���(      .���(                            .�����������.  �8888   .8888                                 ////
////   .08880���088:                                 (888�                                                         .0888�    .0880.                            :88888888888.  .���(   .8888                                 ////
////   �8880.    .:.  .:::       .::.     :::::.   .:�8880:::       .::::.       .::.  .::::     .:::.              .08880  .0880.                             :8888           :::.   .8888        :::::.         .::::.    ////
////   �8880:         �888(     .0880  .�0888888�  �888888888:   .�08888880�.    �880:�088880(.�088880�.             .08880:0880.                              :8888          :888�   .8888     :�0888888�:.   .�08888880.  ////
////   .088880�(:.    .0888.    0888. .0880:::::�  :�08880���.  :0880::::0880.   �88880�:�0888880:(08880.             .08888880.                               :8888::::::.   :888�   .8888    0888:::::0880.  0888:::::�.  ////
////    .:�0888880�.   .8880.  :8880  .8880::        (888�     .8880     .888�   �888(.   :8880.   .0888:              :888888:              :::::::.          :88888888888   :888�   .8888   (888:     �888:  0888(:.      ////
////       .::�08880.   08880 .0880.   .088880��.    (888�     (8888888888888�   �888:    :888�     �888:             .08808888.            :88888880          :8888������(   :888�   .8888   �8888888888888:  .�88880��:   ////
////           .0888�   .0888.0888.      ::��8888.   (888�     (8880:::::::::.   �888:    :888�     �888:            (8880..0888:           .:::::::.          :8888          :888�   .8888   �888(:::::::::.    .:(�08880  ////
////   �::.    :0888:    .08808880    .:     :888�   (8880.    .0888:      .:    �888:    :888�     �888:           �8880.  .0888(                             :8888          :888�   .8888   :8880.      ..   ..    .8888  ////
////   8880���08880:      :888880.    :80����0880.   .08880�0:  .08880�����08.   �888:    :888�     �888:          08880.    .08880                            :8888          :888�   .8888    (0888�����08�   00����0888�  ////
////   .:��������:.        08888.     .:�������:.     .������.   .:���������:    (���.    .���(     (���.         (����.      .����.                           .����          .���(    ����     .:��������:.   .�������:.   ////
////                       0888�                                                                                                                                                                                            ////
////                      �8880.                                                                                                                                                                                            ////
////                      ����.                                                                                                                                                                                             ////
////                                                                            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////   ������  : 2.04                                                           ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////   ����    : 18.07.2011                                                     ////
////   ��������: ������ ������� ����� ��� ����������� ������������              ////
////             ������ ������� � ������                                        ////
////                                                                            ////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////
////                                                                            ////
////  �����: haker or ������ ������� ���������                                  ////
////  Mail: vasyasos@gmail.com                                                  ////
////  Site: http://system-x-files.blogspot.com/                                 ////
////                                                                            ////
////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////

/*

--------------------------------------------------------------���� ��������--------------------------------------------------------------

*/

//��� ����� 2D
class Point2D{
   public $X;
   public $Y;
   function __construct($X = 0,$Y = 0){
      $this->X = $X;
      $this->Y = $Y;  
   }
}

//��� ����� 3D
class Point3D{
   public $X;
   public $Y;
   public $Z;
   function __construct($X = 0,$Y = 0,$Z = 0){
      $this->X = $X;
      $this->Y = $Y;
      $this->Z = $Z;  
   }
}

//��� ����� 4D
class Point4D{
   public $X;
   public $Y;
   public $Z;
   public $T;
   function __construct($X = 0,$Y = 0,$Z = 0,$T = 0){
      $this->X = $X;
      $this->Y = $Y;
      $this->Z = $Z;
      $this->T = $T; 
   }
}

//������� �������� ����� 2D
function P2D($X = 0,$Y = 0){
   return new Point2D($X,$Y);
}

//������� �������� ����� 2D
function P3D($X = 0,$Y = 0,$Z = 0){
   return new Point3D($X,$Y,$Z);
}

//������� �������� ����� 4D
function P4D($X = 0,$Y = 0,$Z = 0,$T = 0){
   return new Point4D($X,$Y,$Z,$T);
}

//�������� ����� 2D
function AddP2D( Point2D $P1, Point2D $P2 ){
   return new Point2D( $P1->X + $P2->X, $P1->Y + $P2->Y );
}

//�������� ����� 3D
function AddP3D( Point3D $P1, Point3D $P2 ){
   return new Point3D( $P1->X + $P2->X, $P1->Y + $P2->Y, $P1->Z + $P2->Z );
}

//�������� ����� 4D
function AddP4D( Point4D $P1, Point4D $P2 ){
   return new Point4D( $P1->X + $P2->X, $P1->Y + $P2->Y, $P1->Z + $P2->Z, $P1->T + $P2->T );
}

//��������� ����� 2D
function SubP2D( Point2D $P1, Point2D $P2 ){
   return new Point2D( $P1->X - $P2->X, $P1->Y - $P2->Y );
}

//��������� ����� 3D
function SubP3D( Point3D $P1, Point3D $P2 ){
   return new Point3D( $P1->X - $P2->X, $P1->Y - $P2->Y, $P1->Z - $P2->Z );
}

//��������� ����� 4D
function SubP4D( Point4D $P1, Point4D $P2 ){
   return new Point4D( $P1->X - $P2->X, $P1->Y - $P2->Y, $P1->Z - $P2->Z, $P1->T - $P2->T );
}

// ����� 2D * -1
function AntiP2D( Point2D $P){
   return new Point2D( - $P->X, - $P->Y );
}

//����� 3D * -1
function AntiP3D( Point3D $P){
   return new Point3D( - $P->X, - $P->Y, - $P->Z );
}

//����� 4D * -1
function AntiP4D( Point4D $P){
   return new Point4D( - $P->X, - $P->Y, - $P->Z, - $P->T );
}

// ����� 2D ^ 2
function SqrtP2D( Point2D $P){
   return new Point2D( expow( $P->X ), expow( $P->Y ) );
}

//����� 3D ^ 2
function SqrtP3D( Point3D $P){
   return new Point2D( expow( $P->X ), expow( $P->Y ), expow( $P->Z ) );
}

//����� 4D ^ 2
function SqrtP4D( Point4D $P){
   return new Point2D( expow( $P->X ), expow( $P->Y ), expow( $P->Z ), expow( $P->T ) );
}

//������� ����� 2D
function DivP2D( Point2D $P1, Point2D $P2 ){
   return new Point2D( $P1->X / $P2->X, $P1->Y / $P2->Y );
}

//������� ����� 3D
function DivP3D( Point3D $P1, Point3D $P2 ){
   return new Point3D( $P1->X / $P2->X, $P1->Y / $P2->Y, $P1->Z / $P2->Z );
}

//������� ����� 4D
function DivP4D( Point4D $P1, Point4D $P2 ){
   return new Point4D( $P1->X / $P2->X, $P1->Y / $P2->Y, $P1->Z / $P2->Z, $P1->T / $P2->T );
}

//��������� ����� 2D
function MulP2D( Point2D $P1, Point2D $P2 ){
   return new Point2D( $P1->X * $P2->X, $P1->Y * $P2->Y );
}

//��������� ����� 3D
function MulP3D( Point3D $P1, Point3D $P2 ){
   return new Point3D( $P1->X * $P2->X, $P1->Y * $P2->Y, $P1->Z * $P2->Z );
}

//��������� ����� 4D
function MulP4D( Point4D $P1, Point4D $P2 ){
   return new Point4D( $P1->X * $P2->X, $P1->Y * $P2->Y, $P1->Z * $P2->Z, $P1->T * $P2->T );
}

//����� ���������� ����� 2D
function SumP2D( Point2D $P ){
   return $P->X + $P->Y; 
}

//����� ���������� ����� 2D
function SumP3D( Point3D $P ){
   return $P->X + $P->Y + $P->Z; 
}

//����� ���������� ����� 4D
function SumP4D( Point4D $P ){
   return $P->X + $P->Y + $P->Z + $P->T; 
}

//������� ���������� ����� 2D
function RazP2D( Point2D $P ){
   return $P->X - $P->Y; 
}

//������� ���������� ����� 2D
function RazP3D( Point3D $P ){
   return $P->X - $P->Y - $P->Z; 
}

//������� ���������� ����� 4D
function RazP4D( Point4D $P ){
   return $P->X - $P->Y - $P->Z - $P->T; 
}

//��� �����
class Line2D{
   public $P1;
   public $P2;
   function __construct(Point2D $P1, Point2D $P2){
      $this->P1 = $P1;
      $this->P2 = $P2;  
   }
   function Point4D(Point4D $P){
      $this->P1 = P2D($P->X,$P->Y);
      $this->P2 = P2D($P->Z,$P->T);       
   }
}

//��������� ��� ����� � �����
function L2D(Point2D $P1,Point2D $P2){
   return new Line2D($P1,$P2);
}

//��� �������
class Box2D{
   public $Pos;
   public $Size;
   function __construct(Point4D $PXYWH){
      $this->Pos = P2D($PXYWH->X,$PXYWH->Y);
      $this->Size = P2D($PXYWH->Z,$PXYWH->T); 
   }   
}

//��� ����������
class Circle2D{
   public $Pos;
   public $R;
   function __construct(Point3D $PXYR){
      $this->Pos = P2D($PXYR->X,$PXYR->Y);
      $this->R = $PXYR->Z;  
   } 
}

/*

---------------------------------------------------------��������������� �������---------------------------------------------------------

*/

//����������� ������� � �����
function ToPoint($obj){
   return new Point2D($obj->x+$obj->w/2,$obj->y+$obj->h/2);
}

//����������� ������� � box
function ToBox($obj){
   return new Box2D(P4D($obj->x,$obj->y,$obj->w,$obj->h));
}

//����������� ������� � ���������� (������ ��� ����� ������, ����� ������ - ��������� ����������)
function ToCircle($obj){
   $dw = $obj->w/2;
   $dh = $obj->h/2;
   if($obj->w < $obj->h)
      $r = $dw;
   else
      $r = $dh;
   return new Circle2D(P3D($obj->x+$dw,$obj->y+$dh,$r));
}

//����������� box � ���������� 
function BoxToCircle(Box2D $box){
   $dw = $box->Size->X/2;
   $dh = $box->Size->Y/2;
   if($box->Size->X < $box->Size->Y)
      $r = $dw;
   else
      $r = $dh;
   return new Circle2D(P3D($box->Pos->X+$dw,$box->Pos->Y+$dh,$r));
}

//����������� ������� � ���� 
function CircleToBox(Circle2D $cir){
   return new Box2D(P4D($cir->Pos->X-$cir->R,$cir->Pos->Y-$cir->R,$cir->R*2,$cir->R*2));
}

//������� ������� �� �����
function BoxToPoints(Box2D $box){
   $p1 = P2D($box->Pos->X,$box->Pos->Y);
   $p2 = P2D($box->Pos->X+$box->Size->X,$box->Pos->Y);
   $p3 = P2D($box->Pos->X+$box->Size->X,$box->Pos->Y+$box->Size->Y);
   $p4 = P2D($box->Pos->X,$box->Pos->Y+$box->Size->Y);
   return P4D($p1,$p2,$p3,$p4);
}

//������� ������� �� �����
function BoxToLines(Box2D $box){
   $p1 = P2D($box->Pos->X,$box->Pos->Y);
   $p2 = P2D($box->Pos->X+$box->Size->X,$box->Pos->Y);
   $p3 = P2D($box->Pos->X+$box->Size->X,$box->Pos->Y+$box->Size->Y);
   $p4 = P2D($box->Pos->X,$box->Pos->Y+$box->Size->Y);
   return P4D(L2D($p1,$p2),L2D($p2,$p3),L2D($p3,$p4),L2D($p4,$p1));
}

function expow($n){
   return pow( $n, 2 );   
}

/*

----------------------------------------------------------������� ������������-----------------------------------------------------------

*/

abstract class Col2D{
   /*
               P O I N T ' s
   */
   
   //����� � �������
   static function PointInBox( Point2D $P, Box2D $BOX ){   
      return ( $P->X > $BOX->Pos->X ) && ( $P->X < $BOX->Pos->X + $BOX->Size->X ) && ( $P->Y > $BOX->Pos->Y ) && ( $P->Y < $BOX->Pos->Y + $BOX->Size->Y );
   }
   
   //����� � ����������
   static function PointInCircle( Point2D $P, Circle2D $CIRC ){
      return SumP2D( SqrtP2D( SubP2D( $CIRC->Pos, $P ) ) ) <= expow( $CIRC->R );
   }
   
   //����� ��� ������������
   static function PointOutBox( Point2D $P, Box2D $BOX ){   
      return !Col2D::PointInBox( $P, $BOX );
   }
   
   //����� ��� ����������
   static function PointOutCircle( Point2D $P, Circle2D $CIRC ){
      return !Col2D::PointInCircle( $P, $CIRC );
   }
   
   /*
               L I N E ' s
   */
   
   //������������ ����� � ������ 
   static function Line( Line2D $A, Line2D $B ){
      $S1 = AntiP2D( SubP2D( $A->P2, $A->P1 ) );
      $S2 = SubP2D( $B->P2, $B->P1 );
      
      $tmp = 1 / ( $S2->X * $S1->Y - $S1->X * $S2->Y );
      
      $tg = SubP2D( $A->P1, $B->P1 );
      
      $s = ( $tg->X * $S1->Y - $S1->X * $tg->Y ) * $tmp;
      $t = ( $S2->X * $tg->Y - $tg->X * $S2->Y ) * $tmp;
      
      global $lS,$lT;
      $lS = $s;
      $lT = $t;
      
      return ( ( $s >= 0 ) && ( $s <= 1 ) && ( $t >= 0 ) && ( $t <= 1 ) );   
   }
   
   //������������ ����� � ��������������� 
   static function LineVsBox( Line2D $A, Box2D $BOX ){
      $lns = BoxToLines($BOX);
      return Col2D::Line( $A , $lns->X ) || Col2D::Line( $A , $lns->Y ) || Col2D::Line( $A , $lns->Z ) || Col2D::Line( $A , $lns->T );
   }
   
   //������������ ����� � �����������  
   static function LineVsCircle( Line2D $L, Circle2D $CIRC ){
      $p1 = SubP2D( $L->P1, $CIRC->Pos ); 
      $p2 = SubP2D( $L->P2, $CIRC->Pos );      
      $d = SubP2D( $p2, $p1 );            
      $a = SumP2D( SqrtP2D( $d ) );
      $b = SumP2D( MulP2D( $p1, $d ) ) * 2;
      $c = SumP2D( SqrtP2D( $p1 ) ) - expow( $CIRC->R );
      
      if ( -$b < 0 )
         return ( $c < 0 );
      else
         if ( -$b < ( 2 * $a ) )
            return ( ( 4 * $a * $c - expow($b) ) < 0 );
         else
            return ( ( $a + $b + $c ) < 0 );
   }
   
   /*
               A A B B ' s
   */
   
   //������������ ���� �������   
   static function Box( Box2D $BOX1, Box2D $BOX2 ){
      return ( $BOX1->Pos->X + $BOX1->Size->X  >= $BOX2->Pos->X ) && ( $BOX1->Pos->X <= ( $BOX2->Pos->X + $BOX2->Size->X ) ) && ( ( $BOX1->Pos->Y + $BOX1->Size->Y ) >= $BOX2->Pos->Y ) && ( $BOX1->Pos->Y <= ( $BOX2->Pos->Y + $BOX2->Size->Y ) );
   }
   
   //������� � ������ �������   
   static function BoxInBox( Box2D $BOX1, Box2D $BOX2 ){
      return ( $BOX1->Pos->X > $BOX2->Pos->X ) && ( ( $BOX1->Pos->X + $BOX1->Size->X ) < ( $BOX2->Pos->X + $BOX2->Size->X ) ) && ( $BOX1->Pos->Y > $BOX2->Pos->Y ) && ( ( $BOX1->Pos->Y + $BOX1->Size->Y ) < ( $BOX2->Pos->Y + $BOX2->Size->Y ) );
   }
   
   //������� � ����������
   static function BoxInCircle( Box2D $BOX, Circle2D $CIRC ){
      $ps = BoxToPoints($BOX);
      return Col2D::PointInCircle( $ps->X, $CIRC ) && Col2D::PointInCircle( $ps->Y, $CIRC ) && Col2D::PointInCircle( $ps->Z, $CIRC ) && Col2D::PointInCircle( $ps->T, $CIRC );
   }
   
   //������� ��� ���������� 
   static function BoxOutCircle( Box2D $BOX, Circle2D $CIRC ){
      return !Col2D::BoxInCircle( $BOX, $CIRC );
   }
   
   //������� ��� ������� �������  
   static function BoxOutBox( Box2D $BOX1, Box2D $BOX2 ){
      return !Col2D::BoxInBox( $BOX1, $BOX2 );
   }
   
   //������������� ������� � ����������� 
   static function BoxVsCircle( Box2D $BOX, Circle2D $CIRC ){
      $lns = BoxToLines($BOX);
      return Col2D::LineVsCircle( $lns->X, $CIRC ) || Col2D::LineVsCircle( $lns->Y, $CIRC ) || Col2D::LineVsCircle( $lns->Z, $CIRC ) || Col2D::LineVsCircle( $lns->T, $CIRC );
   }   
   
   
   /*
               C I R C L E ' s
   */
   
   //������������ ����������� 
   static function Circle( Circle2D $CIRC1, Circle2D $CIRC2 ){      
      return SumP2D( SqrtP2D( SubP2D( $CIRC1->Pos, $CIRC2->Pos ) ) ) <= expow( $CIRC1->R + $CIRC2->R );
   }
   
   //���������� ������ ����������
   static function CircleInCircle( Circle2D $CIRC1, Circle2D $CIRC2 ){
      return SumP2D( SqrtP2D( SubP2D( $CIRC1->Pos, $CIRC2->Pos ) ) ) <= expow( $CIRC1->R - $CIRC2->R );
   }
   
   //���������� � ���������������� 
   static function CircleInBox( Circle2D $CIRC, Box2D $BOX ){
      return Col2D::BoxInBox( CircleToBox( $CIRC ), $BOX );
   }
   
      //���������� ��e ����������
   static function CircleOutCircle( Circle2D $CIRC1, Circle2D $CIRC2 ){
      return !Col2D::CircleInCircle( $CIRC1, $CIRC2 );
   }
   
   //���������� ��� ���������������� 
   static function CircleOutBox( Circle2D $CIRC, Box2D $BOX ){
      return !Col2D::CircleInBox( $CIRC, $BOX );
   }
   
   /*
   
   ----------------------------------------------------------������� ��������� �������������� ����������-----------------------------------------------------------
   
   */
   
   /*
               L I N E ' s
   */
   
   static function LastX(){
      return $GLOBALS['lX'];  
   }
   
   static function LastY(){
      return $GLOBALS['lY'];  
   }
   
   static function LastP2D(){
      return P2D( $GLOBALS['lX'], $GLOBALS['lY'] );  
   }
   
   static function CalcLineCross( Line2D $A, Line2D $B ){
      global $lS,$lT,$lX,$lY;
      $S1 = SubP2D( $A->P2, $A->P1 );
   
      $lX = $A->P1->X + $lT * $S1->X;
      $lY = $A->P1->Y + $lT * $S1->Y;
   }
   
}

?>