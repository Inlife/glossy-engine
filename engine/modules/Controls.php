<?php


class GL_KeyBoard extends GL_Body {

    public $Keys;
    
    public function __construct($Obj) {
    
        parent::__construct($Obj);
        $this->Keys = array();
        
    }
    
    public function Add($Key) {
    
        $this->Keys[$Key] = true;
        switch($this->Parent->Status) {
        
            case GL_MainGame:
            
                if ($Key == VK_SPACE) {
                
                    $this->Parent->EngineDump();
                
                }
            
            break;
            
        }        
        
    }
    
    public function Del($Key) {
    
        $this->Keys[$Key] = false;
        
    }
    
    public function Run() {
    
        $Obj = $this->Parent->Objects[$this->Parent->Player];
        if ($this->Keys) {
        
            switch($this->Parent->Status) {
        
              case GL_MainGame:
              
                if ($this->Keys[VK_RETURN]) {
                
                    $Obj->X = 300;
                    $Obj->Y = 300;
                    $this->Parent->Camera->SetTarget($Obj);
                
                }if ($this->Keys[VK_LEFT]) {
                
                    $Obj->Walk(-1);
                
                }elseif ($this->Keys[VK_RIGHT]) {
            
                    $Obj->Walk(1);
                
                }if ($this->Keys[VK_UP]) {
            
                    $Obj->Jump();
                
                }elseif ($this->Keys[VK_DOWN]) {
            
                    //$Obj->Walk(0, 1);
                
                }if ($this->Keys[100]) {
            
                    $this->Parent->Camera->X -= 4;
                
                }elseif ($this->Keys[102]) {
            
                    $this->Parent->Camera->X += 4;
                
                }elseif ($this->Keys[104]) {
            
                    $this->Parent->Camera->Y -= 4;
                
                }elseif ($this->Keys[98]) {
            
                    $this->Parent->Camera->Y += 4;
                
                }
                
              break;  
                
            }
            
        }
        
    }    

}

class GL_Mouse extends GL_Body {

    public $Keys;
    
    public function __construct($Obj) {
    
        parent::__construct($Obj);
        $this->Keys = array();
        
    }
    
    public function Add($Key, $X, $Y) {
    
        $this->Keys[$Key] = true;
        
    }
    
    public function Del($Key, $X, $Y) {
    
        $this->Keys[$Key] = false;
        
    }
    
    public function Move($X, $Y) {
    
        $this->X = $X;
        $this->Y = $Y;
    
    }
    /*
    public function Run() {
       
        if ($this->Keys) {
        
            if ($this->Keys[LMB]) {
            
                //
                
            }elseif ($this->Keys[RMB]) {
            
                //
                
            }
        
        }
        
    }  */ 

}


?>