<?php
OE_ShowCursor(TRUE);
ini::open("config.ini");
ini::read("Main", "resx", $resX);
ini::read("Main", "resy", $resY);
define(RESOLUT_X, $resX);
define(RESOLUT_Y, $resY);
c('Form1')->W = RESOLUT_X;
c('Form1')->H = RESOLUT_Y;
include('engine/ModuleManager.php');

//dl('php_gd2.dll');

class GL_Engine extends GL_Body {

    public $Layers;
    public $MapH;    
    public $MapW;
    public $Rotation;
    public $Sprites;
    public $Objects;
    public $Sounds;
    public $CheckPoints;
    public $M;

    public function __construct() {
        
        $this->Layers = array();
        $this->Status = GL_MainGame;
        $this->ResX = RESOLUT_X;
        $this->ResY = RESOLUT_Y;
        $this->ZOOM = 1.0;
        $this->Sprites = array();
        $this->Objects = array();
        $this->Sounds = array();
        $this->CheckPoints = array(); 
        $this->M = array();
        $this->M['mainBalckALP'] = 255;
        $this->M['mainBalckReturn'] = false; 
        $this->M['mainBalckTimer'] = 100;
        $this->M['logo_time'] = 500;
        $this->M['MAP_xOFFSET'] = 0;
        $this->M['MAP_yOFFSET'] = 0; 
        $this->Camera = new GL_Camera($this);
        $this->Physics = new GL_Physics($this);
        $this->KeyBoard = new GL_KeyBoard($this);
        $this->Mouse = new GL_Mouse($this);
        $this->Text = new GL_Text($this);
        $this->Loader = new GL_Loader($this);
        $this->Loader->LoadMap('smallcity');  
        //$this->Loader->LoadSprite('File:gfx/GUI/cursors/cur_1.tga,W:40,H:40');  
        
    }
    
    public function Work() {
    
        $GL_TIMER = microtime();
    
        $this->Layers = array();
        $this->KeyBoard->Run();
        $this->BlackScreen();   
        //$cur = $this->Sprites['cur_1'];
        //$this->Image(false, $cur->Texture, $this->Mouse->X, $this->Mouse->Y, $cur->W, $cur->H, 20);
        switch($this->Status) {
        
            case GL_Logo1: 
            
            break;
            case GL_Logo2:
            
            break;
            case GL_MainGame:
            
                
                $this->Camera->CheckFrame();
                $this->Code("Pr2D_Rect( ".(0 - $this->Camera->X).", ".(0 - $this->Camera->Y).", ".$this->MapW.", ".$this->MapH.", 255, 255, PR_DEFAULT)", 7);         
                
                /* BG drawing */
                
                $bg = $this->Sprites['street_1'];
                for ($bgX = 0; $bgX < $this->MapW; $bgX += 1024) {
                
                    $this->Image(false, $bg->Texture, $bgX - $this->Camera->X, 0 - $this->Camera->Y, 1024, 800, 0);
                
                } 
                
                             
                /* OBJ Drawing */ 
                  
                foreach($this->Objects as $Num => $Obj) {    
                                        
                    
                    $sum = $this->Camera->X - $Obj->X;
                    if (($sum < 100) AND ($sum > (-1 * ($this->ResX + 100))))  {
                        $sum = $this->Camera->Y - $Obj->Y;
                        if (($sum < 100) AND ($sum >  (-1 * ($this->ResY + 100))))  {
                                                     
                        
                                $this->Physics->Process($Obj);
                                $sumX = $Obj->X - $this->Camera->X - $this->M['MAP_xOFFSET'];
                                $sumY = $Obj->Y - $this->Camera->Y - $this->M['MAP_yOFFSET'];
                                /* ����� �������� ��� ������� */
                                                   
                                    $this->Code("Pr2D_Line( ".($sumX - $Obj->W / 2).", ".($sumY - $Obj->H / 2).", ".($sumX + $Obj->W / 2).", ".($sumY - $Obj->H / 2).", 255, 255, PR_DEFAULT)", 6);
                                    $this->Code("Pr2D_Line( ".($sumX + $Obj->W / 2).", ".($sumY - $Obj->H / 2).", ".($sumX + $Obj->W / 2).", ".($sumY + $Obj->H / 2).", 255, 255, PR_DEFAULT)", 6);
                                    $this->Code("Pr2D_Line( ".($sumX + $Obj->W / 2).", ".($sumY + $Obj->H / 2).", ".($sumX - $Obj->W / 2).", ".($sumY + $Obj->H / 2).", 255, 255, PR_DEFAULT)", 6);
                                    $this->Code("Pr2D_Line( ".($sumX - $Obj->W / 2).", ".($sumY + $Obj->H / 2).", ".($sumX - $Obj->W / 2).", ".($sumY - $Obj->H / 2).", 255, 255, PR_DEFAULT)", 6); 
                            
                                $this->Code("Pr2D_Pixel( ".$sumX.", ".$sumY.", 255, 255, PR_DEFAULT)", 7);                             
                                $S = $Obj->Sprite;    
                                if ($S) {
                                    
                                    if ($S->Animated) {
                                    
                                        $this->Animate($Obj);      
                                                                       
                                    }                                                                         
                                    $this->Image($S->Animated, $S->Texture, $sumX - $S->W / 2 - $S->X, $sumY - $Obj->H / 2 - $S->Y, $S->W,  $S->H, 5, $S->Frame); 

                                }
                                                        
                        }
                             
                    }  
                                                                                              
                }
                
                $this->PlayerInfo();
                
            break; 
        }    
        $GL_TIMER_PREV = $this->M['GL_TIMER_PREV'];
        $GL_TIMER_CURRENT = round(microtime() - $GL_TIMER, 6);
        $GL_TIMER_AVERAGE = round(($GL_TIMER_PREV + $GL_TIMER_CURRENT) / 2, 6);
        Utils::Bar($GL_TIMER_CURRENT . ' - ' . $GL_TIMER_AVERAGE);
        $this->M['GL_TIMER_PREV'] = $GL_TIMER_AVERAGE + 0;    
    
    }
    
    public function PlayerInfo() {
    
        $Obj = $this->Objects[$this->Player];
        $this->Text->String('��������:', 0, 0, 15);
        $this->Text->String($Obj->XSpeed, 120, 0, 15);
        $this->Text->String('Hello ���������� ��� ! =)', 0, 300, 15, 20, 15); 
    
    }
    
    public function EngineDump() {
    
        //$this->Loader->LoadGame();
        pre($this);
    
    } 
    
    
    public function Image($A, $TEX, $X, $Y, $W, $H, $LAYER, $FRAME = 1, $ALPHA = 255, $ANGLE = 0, $FLAG = FX_DEFAULT) {
        
        switch($A) {
        
            case false:
                            
                $this->Code("SSprite_Draw(".$TEX.", ".$X.", ".$Y.", ".($W * $this->ZOOM).", ".($H * $this->ZOOM).", ".$ALPHA.", ".$ANGLE.", ".$FLAG.")", $LAYER);
            
            break;
            case true:
            
                $this->Code("ASprite_Draw(".$TEX.", ".$X.", ".$Y.", ".($W * $this->ZOOM).", ".($H * $this->ZOOM).", ".$FRAME.", ".$ALPHA.", ".$ANGLE.", ".$FLAG.")", $LAYER);
            
            break;
            
        }        
        
    }
    
    public function Animate($Obj) {  
                                                                                          
        if (!$Obj->Sprite->Animated) {
        
            return false;
            
        }
        
        if ($Obj->Sprite->StopT <= 0) {
        
            $Obj->Sprite->Status(A_STOP);                               
            
        }else{
        
            $Obj->Sprite->StopT -= GL_OBJ_STOPT_MIN;   
                     
        }
        
        switch($Obj->dirX) {
        
            case 0:
            case 1:
            $FRAMES = $Obj->Sprite->AnimMap[$Obj->Sprite->Status][0]; 
            if ($Obj->Sprite->CountTime <= 0) {
            
                if ($Obj->Sprite->Frame >= $FRAMES[1]) {
                
                    $Obj->Sprite->Frame = $FRAMES[0];
                    
                }elseif ($FRAMES[0] != $FRAMES[1]){
                
                    $Obj->Sprite->Frame += 1;
                    
                }
                
                $Obj->Sprite->CountTime = $Obj->Sprite->AnimDelay;
                
            }else{
            
                $Obj->Sprite->CountTime -= GL_OBJ_COUNTTIMEMIN;     
                           
            }    
            
            break;
            case -1:    
                    
            $FRAMES = $Obj->Sprite->AnimMap[$Obj->Sprite->Status][1];               
            if ($Obj->Sprite->CountTime <= 0) {
            
                if ($Obj->Sprite->Frame <= $FRAMES[0]) {
                
                    $Obj->Sprite->Frame = $FRAMES[1];
                    
                }elseif ($FRAMES[0] != $FRAMES[1]) {  
                 
                    $Obj->Sprite->Frame -= 1;
                    
                }
                
                $Obj->Sprite->CountTime = $Obj->Sprite->AnimDelay;
                
            }else{
            
                $Obj->Sprite->CountTime -= GL_OBJ_COUNTTIMEMIN;    
                            
            }
            
            break;
            
        }         
        
    }
    
    public function Status($status) {
    
        $this->M['nextStatus'] = $status;
        $this->BlackScreen(true);
        
    }
    
    public function BlackScreen($reset = false) {
    
        if ($reset) {
        
            $this->M['mainBalckReturn'] = true;    
                                                                              
        }
        
        if (($this->M['mainBalckReturn']) AND ($this->M['mainBalckALP'] < 253)) {
        
            $this->M['mainBalckALP'] += 3;
            
        }elseif((!$this->M['mainBalckReturn']) AND ($this->M['mainBalckALP'] > 3)) {
        
            $this->M['mainBalckALP'] -= 3;
            
        }elseif ($this->M['mainBalckALP'] > 253) {
        
            $this->M['mainBalckALP'] = 252;
            $this->Status = $this->M['nextStatus'];
            $this->M['mainBalckReturn'] = false;
            
        } 
        $ALP = $this->M['mainBalckALP'];
        if ($ALP > 1) {
        
            $this->Code("Pr2D_Rect(0, 0, ".$this->ResX.", ".$this->ResY.", 0, ".$ALP.", PR_FILL)", 15);
            
        }   
          
    }  
    
    public function Code($code, $Layer) {
    
        $code = ' '.$code."; \n";
        $this->Layers[$Layer] .= $code;
        
    }

    public function Draw() {
    
        $_CODE = 'global $GL; ';
        ksort($this->Layers);
        $_CODE .= implode(' ', $this->Layers);
        //file_put_contents('code.txt', $_CODE);
        eval($_CODE);
        
    }
    
    public function KeyBoardAdd($key) {
    
        $this->KeyBoard->Add($key);
    
    }
    
    public function KeyBoardDel($key) {
    
        $this->KeyBoard->Del($key);
    
    }
    
    public function MouseAdd($key, $x, $y) {
    
        $this->Mouse->Add($key, $x, $y);
    
    }
    
    public function MouseDel($key, $x, $y) {
    
        $this->Mouse->Del($key, $x, $y);
    
    }
    
    public function MouseMove($x, $y) {
    
        $this->Mouse->Move($x, $y);
    
    }

}


?>