<?php

// Dirs
define(GL_ENGINE_DIR, 'engine/');     
define(GL_GFX_DIR, 'gfx/');
define(GL_SAVE_DIR, 'saves/');
define(GL_DATA_DIR, 'data/'); 
define(GL_CACHE_DIR, 'cache/');           
define(GL_GUI_DIR, GL_GFX_DIR . 'GUI/');
define(GL_MODULE_PATH, GL_ENGINE_DIR . 'modules/');
define(GL_MAPS_DIR, GL_DATA_DIR . 'maps/');
define(GL_FONT_DIR, GL_GUI_DIR . 'fonts/');

// Names
define(GL_CACHE_INAME, 'tcache_');
define(GL_CACHE_IFILETYPE, '.gif');
define(GL_FONTNAME, GL_FONT_DIR . 'arial.ttf');

// Engine Statuses
define(GL_Logo1, 1);
define(GL_Logo2, 2);
define(GL_MainGame, 3);

// Sprites
define(GL_ASPRITE, '[A]');
define(GL_SSPRITE, '[S]');

// Objects
define(GL_OPLAYER, '[P]');
define(GL_O, '[O]');
define(GL_OCP, '[CP]');
define(GL_OHUMAN, '[H]');
define(GL_OBJ_STATIC,  'static');
define(GL_OBJ_DYNAMIC, 'dynamic');

// Object Statuses
define(A_STOP, 'A_STOP');
define(A_WALK, 'A_WALK');
define(A_JUMP, 'A_JUMP');

// Object Properties
define(GL_OBJ_JUMPKEYMAX, 12);
define(GL_OBJ_JUMPKEYMIN, 2);
define(GL_OBJ_STOPT, 10);
define(GL_OBJ_STOPT_MIN, 1);
define(GL_OBJ_ANIMDELAY, 3);
define(GL_OBJ_COUNTTIMEMIN, 1);

?>