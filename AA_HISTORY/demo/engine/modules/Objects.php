<?php
 
 
class GL_ObjMain extends GL_Body {

    public function __construct($params = false) {
    
        $this->X = 0;
        $this->Y = 0;
        $this->W = 0;
        $this->H = 0;
        $this->Angle = 0;
        $this->Name = microtime();
        if ($params) {
        
            Utils::str2args($params, $this);
        
        }
    
    }
    
} 

class GL_SSprite extends GL_ObjMain  {

    public function __construct($params) {
    
        parent::__construct();
        $this->Texture = false; 
        $this->OffsetX = 0;
        $this->OffsetY = 0;
        $this->Alpha = -1;
        $this->Frame = 1;
        $this->Flag = TEX_DEFAULT_2D;
        $this->Animated = false;
        Utils::str2args($params, $this);
        $this->Texture = Tex_LoadFromFile($this->File, intval($this->Alpha), $this->Flag);
        unset($this->Alpha, $this->Flag);
        $this->Name = basenameNoExt($this->File);  
    }

}

class GL_ASprite extends GL_SSprite {

    public function __construct($params) {
    
        $this->AnimMap = false;
        $this->StopT = GL_OBJ_STOPT;
        $this->AnimDelay = GL_OBJ_ANIMDELAY;
        $this->Status = A_STOP;
        $this->CountTime = $this->AnimDelay;
        parent::__construct($params);
        $this->Animated = true;
        Utils::AnimMap($this->AnimFile, $this);
        Tex_SetFramesSize($this->Texture, $this->Wcut, $this->Hcut);
        unset($this->Wcut, $this->Hcut);
        $this->Name = basenameNoExt($this->File);      
    
    } 
    
    public function Status($status) {
    
        $this->Status = $status;
        $this->StopT = 4;
        
    } 

}

/**
 * #Start Static Objects
 */  
 
class GL_SObject extends GL_ObjMain {

    public $M;
    
    public function __construct($params) {
        
        parent::__construct();    
        $this->Walkable = false;
        $this->Collision = array();
        $this->Sprite = false;
        $this->dirY = 0;
        $this->dirX = 0;
        $this->M = array();            
        $this->Type = GL_OBJ_STATIC;
        Utils::str2args($params, $this); 
        
    }

}

class GL_CheckPoint extends GL_ObjMain {

    public function __construct($params) {
    
        $this->CPactive = 1; 
        $this->Walkable = true;
        parent::__construct($params);
    
    }  

}

/**
 * #End Static Objects
 * #Start Dynamic Objects
 */ 

class GL_AObject extends GL_SObject {

    public function __construct($params) {
    
        $this->XSpeed = 0;
        $this->YSpeed = 0;
        $this->aX = 1;
        $this->aY = 1;
        $this->Xlimit = 5;
        $this->Ylimit = 5;  
        $this->Jump = false;
        parent::__construct($params);
        $this->Type = GL_OBJ_DYNAMIC;
    
    }
    
    public function Status($status) {
    
        $this->Sprite->Status = $status;
        $this->Sprite->StopT = 10;
        
    }

}

class GL_HumanObject extends GL_AObject {

    public function __construct($params) {
        
        parent::__construct($params);
        $this->XSpeed = 0;
        $this->YSpeed = 0;
        $this->aX = 10;
        $this->aY = 1;
        $this->Xlimit = 10;
        $this->Ylimit = 15; 
        $this->Jump = false;
        $this->Health = 100; 
        $this->Walkable = true;
    
    }
    
    public function Walk($dirX) {
    
        $this->dirX = $dirX;
        $this->Sprite->Status(A_WALK);
    
    }
    
    public function Jump() {
       
        if ($this->M['jumpKey'] == 0) {
        
            $this->M['jumpKey'] = GL_OBJ_JUMPKEYMAX;
            if (!$this->Jump) {
            
                $this->dirY = -1;
                $this->Jump = true;
                $this->Sprite->Status(A_JUMP);
                
            }
            
        }else{
        
            $this->M['jumpKey'] -= GL_OBJ_JUMPKEYMIN;
            
        }
    
    }
    
}

class GL_PlayerObject extends GL_HumanObject {

    public function __construct($params) {
    
        $this->LastCP = false;
        parent::__construct($params);
    
    }

}

/**
 * #End Dynamic Objects
 */ 

?>