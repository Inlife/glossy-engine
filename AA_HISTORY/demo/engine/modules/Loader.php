<?php
/*
 * �� ���������
 */
class GL_Loader extends GL_Body {

    public function __construct($Obj) {
        parent::__construct($Obj);
    }
    
    // file:name,alpha:255,flag:0;    
    
    public function StaticGUI($params) {
    
        /* �������� �������� menu */
        $Sprite = new GL_SSprite($params); 
        $this->Parent->GUI[$Sprite->Name] = $Sprite;
        unset($Sprite);
        
    }

    public function AnimGUI($params) {
    
        /* �������� ������������� �������� menu */
        $Sprite = new GL_ASprite($params); 
        $this->Parent->GUI[$Sprite->Name] = $Sprite;
        unset($Sprite);
        
    }
    
    public function LoadGUISound($params) {
    
        $Sound = new GL_Sound($params); 
        $this->Parent->SUI[$Sound->Name] = $Sound;
        unset($Sound);
        
    }
    
    public function LoadSound($params) {
    
        $Sound = new GL_ASprite($params); 
        $this->Parent->Sounds[$Sound->Name] = $Sound;
        unset($Sound);
        
    }

    public function LoadSprite($params) {
        
        /* �������� �������� */
        $Sprite = new GL_SSprite($params); 
        $this->Parent->Sprites[$Sprite->Name] = $Sprite;
        unset($Sprite);
         
    }

    public function LoadAnimSprite($params) {
        
        /* �������� ������������� �������� */       
        $Sprite = new GL_ASprite($params); 
        $this->Parent->Sprites[$Sprite->Name] = $Sprite;
        unset($Sprite);
         
    }
    
    public function LoadObject($input) {
    
        /* �������� �������� */
        $params = preg_replace("/\[\S+\]/", '', $input);
        if (strstr($input, GL_OPLAYER)) {
        
            $Object = new GL_PlayerObject($params);
            $this->Parent->Player = $Object->Name;
        
        }elseif (strstr($input, GL_OHUMAN)) {
        
            $Object = new GL_HumanObject($params);
        
        }elseif (strstr($input, GL_O)) {
        
            $Object = new GL_SObject($params);
        
        }elseif (strstr($input, GL_CP)) {
        
            $Object = new GL_CheckPoint($params);
        
        }
        //pre($this->Parent->Sprites[$Object->Sprite]);
        if ($Object->Sprite) {
        
            $Object->Sprite = clone $this->Parent->Sprites[$Object->Sprite];
            
        } 
        $this->Parent->Objects[$Object->Name] = $Object;
        unset($Object);
     
    }

    public function LoadMap($lvl) {
    
        /* ������� ����� */
        $this->DestroyMap();
        $file = file_get_contents(GL_MAPS_DIR . $lvl . '.map');
        $BLOCKS = explode('==', $file);
        
        $mapBlock = $BLOCKS[0]; 
        $this->Parent->MapName = $lvl;       
        Utils::str2args($mapBlock, $this->Parent);
                       
        /* �������� �������� */
        $sprBlock = explode(';', $BLOCKS[1]);
        foreach($sprBlock as $params) {                 
            
            if (strstr($params, GL_ASPRITE)) {
            
                $this->LoadAnimSprite(str_replace(GL_ASPRITE, '', $params));
            
            }elseif(strstr($params, GL_SSPRITE)) {
            
                $this->LoadSprite(str_replace(GL_SSPRITE, '', $params));
                
            }    
            
        }          
        /* �������� ��������� */
        $objBlock = explode(';', $BLOCKS[2]);       
        foreach($objBlock as $params) {            
        
            $this->LoadObject($params);   
            
        }         
        $this->Parent->Camera->SetTarget($this->Parent->Objects[$this->Parent->Player]);    
         
    }
    
    /* �������� 
    
    public function CheckPoint($Obj) {
    
        $this->Parent->M['playerPOS']
    
    }    */
    
    public function SaveGame() {
            
        $str = array();
        $text = array();
        $name = 'map_name='.$this->Parent->MapName;
        foreach($this->Parent->Objects as $Object) {
        
            $str = array();
            foreach($Object->Data as $Key => $Value) {
            
                if (!is_object($Value) && !is_array($Value)) {
                
                    $str[] = $Key . ':' . $Value;
                
                }
            
            } 
            
            $text[] = implode(',', $str);          
        
        }
        $text = implode(";\n", $text);
        file_put_contents(GL_SAVE_DIR . 'test1.sav', $name . '==' . $text);
                           
    }
    
    public function LoadGame() {
    
        $file = file_get_contents(GL_SAVE_DIR . 'test1.sav');
        $BLOCKS = explode('==', $file);
        
        $name = explode('=', $BLOCKS[0]);
        $this->LoadMap(trim($name[1]));
        
        $Objects = explode(';', $BLOCKS[1]);
        foreach($Objects as $params) {
        
            $tempObj = new GL_ObjMain($params);
            $Obj = $this->Parent->Objects[$tempObj->Name];
            foreach($tempObj->Data as $Key => $Value) {
            
                $Obj->$Key = $Value;
            
            }
            unset($tempObj);
        
        }
        $this->Parent->Camera->Update(); 
    
    }
    
    public function DestroyMap() {
    
        $this->Parent->MapName = '';
        if (is_array($this->Parent->Objects)) {
        
            foreach($this->Parent->Objects as $num1 => $Obj) {
            
                unset($Obj);
                unset($this->Parent->Objects[$num1]);
                
            }
            
        }    
        /*foreach($this->Sounds['g'] as $num1 => $Obj) {
        
            unset($Obj);
            unset($this->Sounds['g'][$num1]);
            
        }  */
        if (is_array($this->Parent->Sprites)) {
        
            foreach($this->Parent->Sprites as $num2 => $sprite) {
            
                Tex_Free($sprite->Sprite->Texture);
                unset($sprite);
                unset($this->Parent->Sprites[$num2]);
              
            }    
            
        }
        
    }

}

?>