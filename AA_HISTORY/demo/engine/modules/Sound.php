<?php

class GL_Sound  {
    public $File;
    public $Level;
    public $Loop;
    public $Volume;
    public $Pan;
    
    public function __construct($params) {
        $this->File = false;
        $this->Level = 3;
        $this->Loop = false;
        $this->Volume = 100;
        $this->Pan = 50;
        Utils::str2args($params, $this);
        $this->Name = basenameNoExt($this->File);
    }
    
    public function Play() {
        c("GLS".$this->Level)->fileName = $this->File;
        c("GLS".$this->Level)->volume = $this->Volume;
        c("GLS".$this->Level)->pan = $this->Pan;
        c("GLS".$this->Level)->loop = $this->Loop;
        c("GLS".$this->Level)->Play();
    }
    
    public function Stop() {
        c("GLS".$this->Level)->Stop();
    }
    
    public function isPlay() {
        return c("GLS".$this->Level)->isPlay();
    }
    
}

?>